---
franchise: Myst
longname: Riven
copyright: "\u00a9 1997 Cyan, Inc. and Ubi Soft Entertainment"
plugin: scummvm_common
engine: scummvm (>= 1.9.0)
wiki: Riven

packages:
  riven-cd-data:
    provides: riven-data
    mutually_exclusive: True
    longname: "Riven: the Sequel to Myst"
    copyright: "\u00a9 1997 Cyan, Inc. and Ubi Soft Entertainment"
    gameid: riven
    install:
    - common assets
    - cd assets
    optional:
    - patch102
    doc:
    - cd documentation

  riven-dvd-data:
    provides: riven-data
    mutually_exclusive: True
    longname: "Riven: the Sequel to Myst"
    copyright: "\u00a9 1997 Cyan, Inc. and Ubi Soft Entertainment"
    gameid: riven
    install:
    - common assets
    - dvd assets
    - dvd gog assets
    doc:
    - dvd documentation
    license:
    - dvd licenses

  riven-25th-data:
    gog:
      url: riven_the_sequel_to_myst
    provides: riven-data
    mutually_exclusive: True
    longname: "Riven: the Sequel to Myst - 25th Anniversary Edition"
    copyright: "\u00a9 1997-2022 Cyan, Inc. and Ubi Soft Entertainment"
    gameid: riven
    langs: [en,de,es,fr,it,jp,pl,ru]
    install:
    - common assets
    - dvd gog assets
    - gog assets
    doc:
    - gog documentation
    license:
    - gog licenses

files:
  setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114).exe:
    other_parts:
      setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-1.bin
      setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-2.bin
    provides:
    - common assets
    - dvd gog assets
    - gog assets
    doc:
    - gog documentation
    license:
    - gog licenses
    unpack:
      format: innoextract

groups:
  archives:
    group_members: |
      890272    879b8aaa724f4f422661634186ee2534 setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114).exe
      4294085630 dd91f1256d819d6f26d9d0bcaa81548f setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-1.bin
      1197515061 2d03d82b784666472e1a3f436ed20924 setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-2.bin

  common assets:
    group_members: |
      569678    9658363d21012c757ed8ef5412c4f363 a_Sounds.mhk
  
  cd assets:
    group_members: |
      10148976  d6450e937791232c201ab933451815b2 a_Data.mhk?cd
      334753908 ea84fee7c06975b030841706dee24dec b_Data.mhk?cd
      13367590  3b4d8ef0037eb0d53ffafb4ddc084233 b_Sounds.mhk?cd
      401356410 32742619cd38a9c82d6403c9ea8acf2b g_Data.mhk?cd
      17672352  7998b57be99188e03a9ff292e71e169a g_Sounds.mhk?cd
      312808786 cbbfe924f4f64b61901841fab44751b6 j_Data1.mhk?cd
      351121758 310c4f169d96c124122133ae9cafcf36 j_Data2.mhk?cd
      36480544  b27fcc84564db0776b28781ad0a24c2d j_Sounds.mhk?cd
      434167498 53ec0d4c6c683c172b311994529a1ef6 o_Data.mhk?cd
      7767514   c058991c942024cf7fcbe7fc8a4a194e o_Sounds.mhk?cd
      154638574 a1d052cc19d17a52fd34909b859ffc20 p_Data.mhk?cd
      5403024   8381d115e9739344abbf15e19edddb62 p_Sounds.mhk?cd
      72825506  36ef4d193f46415a5d59f6a958f1200d r_Data.mhk?cd
      11162816  827a4009dce6c43d35592b26752c8db2 r_Sounds.mhk?cd
      658519154 9c4201dc16c7878bd47c571af79e35f3 t_Data.mhk?cd
      27807630  e44fc1a958455a5a7076a4cd70b9f30c t_Sounds.mhk?cd
      762689    fae235a3ff26ad32a2b084c22168acd4 arcriven.z?cd

  patch102:
    group_members: |
      1212      fa08d302e0dd0c124203c0b95cc2223f b_Data1.mhk
      50490     173c3b218b9603ab42c0f27546d37b22 j_Data3.mhk

  cd documentation:
    doc: true
    group_members: |
      1725262   762a3acab001fecd382f25cafbbd4902 Manual.pdf?cd

  dvd gog assets:
    group_members: |
      10218888  ad82a3bc2d6a7c2b2ac4dcf7ab95b467 a_Data.mhk?dvd
      341972592 8e7c1091348fe726543c294339379a26 b_Data.mhk?dvd
      14482476  758bd07bf60ae2ef344f82ebcf5096ab b_Sounds.mhk?dvd
      437482182 82badee390a546be3a67f3f12ba50f85 g_Data.mhk?dvd
      9320910   328b8088d4709331444f283ce805b38c g_Sounds.mhk?dvd
      331171018 30b52003e439550b56d27741589760c7 j_Data1.mhk?dvd
      347318358 f64ab7af5de741ff0eb0dbb2f340c5ea j_Data2.mhk?dvd
      19126388  fad09049ae8b9a02952258d42261755d j_Sounds.mhk?dvd
      447978224 c0b76a88bae1201381fb92f860f48634 o_Data.mhk?dvd
      3994764   937ea7c694ac011ffae5318225acafe1 o_Sounds.mhk?dvd
      147399416 f8c8e7d5363ce9bf08f2275438aea9f4 p_Data.mhk?dvd
      2851896   e323b7152b77c6f1a7d0778153593302 p_Sounds.mhk?dvd
      72758778  1dc2a0cb62b54d08ce73bc0926e49640 r_Data.mhk?dvd
      5884780   539203e7d98b3648cdacb0dc08eaaf10 r_Sounds.mhk?dvd
      478427056 e6f2bb5697ba96c1df8c8a92c18ea680 t_Data1.mhk
      243465158 93cdc66d782c624b756911a8c841de72 t_Data2.mhk
      17121806  0d1e41e7be29004637aadf76474ae477 t_Sounds.mhk?dvd
      6546298   e386ace2719cfe36cc94b7e79c829694 b2_data.mhk
      231174    5108d5f01bfda62e41cc9a6516b7c6af Extras.mhk

  dvd assets:
    group_members: |
      609621    fa19802869ac7fbd0db2036fab707597 arcriven.z?dvd

  dvd documentation:
    doc: true
    group_members: |
      535370    dafbe606231b359872b59c135d475196 Manual.pdf?dvd
      19539     e906123203a929e76338c4fc4ff39a93 Readme.txt

  dvd licenses:
    group_members: |
      19083     27a59367cd6954202d05287124583271 Read EULA First
    license: true

  gog assets:
    group_members: |
      714456    f8f89c1c43ac513cdc8451fee0062032 FreeSans.ttf
      7088579   15f60f59a614dc2f447a5d24c378e225 a_data_french.mhk
      7098655   a3ddf5a7ab367a420b8b12b810537937 a_data_german.mhk
      6677740   489715472a29cfb319d757bb7029dce3 a_data_italian.mhk
      7237370   fd0578cc6839584f17d9ed0cb1b502a7 a_data_japanese.mhk
      14588293  0999a57018f0f6a6f4ab8d07152f9665 a_data_polish.mhk
      14349136  47d34f1f3ea154893b099f0387a58e55 a_data_russian.mhk
      8133297   713a2a44e8e13b00206d10644115017a a_data_spanish.mhk
      8012153   5757e3965a2af73469149396996d5ee4 b_data_french.mhk
      8049710   55e857966ada6bef6080c26e4b9f01c4 b_data_german.mhk
      7878469   fcef2f298b85627f85acaff8e2f6422d b_data_italian.mhk
      8051720   dc66278db53b1d31572e9ba4085300b3 b_data_japanese.mhk
      8736448   c6b4ce3923f4003c088c6cd29661a9c0 b_data_polish.mhk
      5678169   6ab5bf95255d8f8f2a2688c0b028b984 b_data_russian.mhk
      8152410   03f47bc5a4c0ada069c50f4910602f1b b_data_spanish.mhk
      2048456   0701a3d47840e8c2ca2084670fbc9129 g_data_polish.mhk
      9670323   e6c2c4b60051a04c2d7f1cbba8db63fd j_data_french.mhk
      9670311   fe8784baab4e2b9b7cab498c0d3ed1e3 j_data_german.mhk
      7928309   5be7f0ff78e43f354eabf66e6aa3a049 j_data_italian.mhk
      9672327   f7c4f7620010d44067d14c996b62fadd j_data_japanese.mhk
      9670255   91f2ff1b58a125672be86b309f4c87e8 j_data_spanish.mhk
      1744108   05d68888fff2a32088fb5b84cccd6cd2 mplus-2c-regular.ttf
      307743560 6e8fec12ef6fc4280626df95efe4c26c o_data_french.mhk
      307757733 7e402f1f05ee9d54d73062b8131bfc3e o_data_german.mhk
      305381997 2a8a5e8085644f5ad5ea5c94e0841b45 o_data_italian.mhk
      306380205 8db91fac713e7f93c685470968024d79 o_data_japanese.mhk
      307857737 4d3260a59dae051d4f7ba5d58a405050 o_data_polish.mhk
      299099114 ec1741f64149bb8f931e75ca591dacb4 o_data_russian.mhk
      307863059 62523535eb79a46079cbf250bb39f32c o_data_spanish.mhk
      82649377  ea233ef538077d5cac9aff4d3de1482f p_data_french.mhk
      82649377  4c4bcf7bef97fbc02d19021130675e32 p_data_german.mhk
      90005241  77ffa323ff4290e03bba1fff36e60603 p_data_italian.mhk
      82643503  c2baeb94c50f7d79210ac473a5e3dd71 p_data_japanese.mhk
      87475435  cad2d396124a240672594c44e0123584 p_data_polish.mhk
      65807778  812c7f04fd7290345f9998c1892d4a5f p_data_russian.mhk
      82629765  d71a8c12f89395ddafb864c6f2e8cb8e p_data_spanish.mhk
      11169460  56dc21fd09f2e987752a7c0f46e6c969 r_data_french.mhk
      11169460  1403b50dea173e1d9048e8018ff5d888 r_data_german.mhk
      11169460  7fa0960f0e06e853582007f3f19e1d91 r_data_italian.mhk
      11169304  a7f2282ee043c05e56de1c7c0b49da4a r_data_japanese.mhk
      13852587  71304221aa1d6a8c195d9ddf7744f25d r_data_polish.mhk
      11168476  cfc16e37327e1e75b960eeb33350eac8 r_data_russian.mhk
      11169460  c50bc1c87fdd574baedd659769df0a12 r_data_spanish.mhk
      878592    6bf4e493923b9c06abcb7d9b8d6ca037 riven.exe
      155423616 86f31568d1fa2563d18f0464129ea2d8 t_data_french.mhk
      155423616 90ea9c88640937f2298f5c93fe5263e4 t_data_german.mhk
      153780813 d6d6c18ac981d698d6e0dfaeba376e97 t_data_italian.mhk
      158238346 57898ad65842fbcb16393d867a80568e t_data_japanese.mhk
      159367762 ff3cc0443ae6a4b1c7fc37c13abc7ed0 t_data_polish.mhk
      185250940 8d7e553b247eebd397a81d5308d7d791 t_data_russian.mhk
      155384296 55b01e4ce365c4d7120c468e97f640a5 t_data_spanish.mhk

  gog documentation:
    doc: true
    group_members: |
      1860037   8b88bbfe16feba3ed8e60746b9acc161 manual.pdf?gog
      128211    46875b57c08bf2ba11cc3e0e751e3e70 readme.txt?gog

  gog licenses:
    group_members: |
      43163     3ab82c74ab2fa83a173f634e78a46942 EULA_de-DE.txt?gog
      36755     d956462aed1a0e2bce34d1b60abfca37 EULA_en-US.txt?gog
      45134     695caacdc4fe96b9b02302c663fe7a9e EULA_fr-FR.txt?gog
      39514     6f543bb234f2f7ae4a6716944af345e2 EULA_pl-PL.txt?gog
      41049     756115e3e6b96ad57148eb6e306dc260 EULA_pt-BR.txt?gog
      73635     e468783b617fbd93b7bca9a9b96896c1 EULA_ru-RU.txt?gog
      35129     cfd5c510c75ebda3ec28fd764e9577d9 EULA_zh-Hans.txt?gog
      347       c75344e0a4f880dd5fe056b5b86deb96 LICENSE.mplus
    license: true


sha1sums: |
  899074730a659d784d0022773345a23b67dd1f1e setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114).exe
  f167347a39d028f6189f2fbf7dbd2545a5091081 setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-1.bin
  a05b2b0c8b1519875db4dbd1aa3ef7b6d99c3172 setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-2.bin
  7ee5c573e3d3c1a8101befa7d89224c462c33554 FreeSans.ttf
  a70b05899e79e96734d62e59093e646812eba2db LICENSE.mplus
  86abfaa5cf206724229e2b6f82687eec6d95cf75 a_Data.mhk?cd
  f615a29b5bc9054d3232c7c49b43fa6ab5e37f67 a_data_french.mhk
  70dccdea05393f6234cd7278234ef9459621126b a_data_german.mhk
  70efdfe82d0e232d28fad50cf1e8cc80e6921c25 a_data_italian.mhk
  0f9625262351d696ff20304ae827efd1ea715dab a_data_japanese.mhk
  753e7970fc747a4b60caaed892e0eb9d2ac7ccb0 a_data_polish.mhk
  a8c1ee6afec3de9a712f95bc8cf81241c85f52ee a_data_russian.mhk
  adbd25470a01f6987c7a9781e7ce494e3527fb5f a_data_spanish.mhk
  245349b5a0715406198aaaeabe26fdeae732e92f a_Sounds.mhk
  917480dd6f931b3cac324f63efa370a6a971c120 b_Data.mhk?cd
  25d44582f26ba11b974eb32772358288e541223d b_data_french.mhk
  92cf4d13f09bc577d1d530feb7c8526bc985c91d b_data_german.mhk
  e89ff2c404aa792110f03c76044ff1c5552acb5c b_data_italian.mhk
  1cdc97d579485396c68a549b9209f9baf56654f6 b_data_japanese.mhk
  217c1fafa57e0b33775208e0eab82f3c3ee403fa b_data_polish.mhk
  234fe5c52751d428f658a441b3daa0cdf62077f5 b_data_russian.mhk
  db628f6fa1620f99c71182ef77b0b90c544a1796 b_data_spanish.mhk
  00cf01b77408bba91332965e80da374cbb75c3e6 b_Sounds.mhk?cd
  aab4876fb74693ca1608db62cf6877a3a1d534e2 g_Data.mhk?cd
  e05d0053b50e651d6b9c549fc0ae12783605aa7d g_data_polish.mhk
  2eaae89538fb2f2d65e5ec7247b98cb782d070c5 g_Sounds.mhk?cd
  f9b2d9dfbd9ac5229a260d3b0b2769cf318f73e2 j_Data1.mhk?cd
  07d3deb6177554909ce28d48f5e4493b693fb803 j_Data2.mhk?cd
  62a02968022500377a3a58139e74ef54b2e95862 j_data_french.mhk
  dd31d64353bdd5aad5ac074ad5de8e2b68a8a33a j_data_german.mhk
  7ba1e0b6969cfd65471505155fd2592b5e20acbc j_data_italian.mhk
  23b98552dd8d7c9adfb4657ff57671baf96270ea j_data_japanese.mhk
  a33159385f3ab31408eb9ddc334debc30cef3f67 j_data_spanish.mhk
  4a38695c974d7bc89cd1c8bf37b2d733db43947c j_Sounds.mhk?cd
  b9e748e44612e0597ea60a5dfc2bf61cb7c0d194 mplus-2c-regular.ttf
  a9942c8d45a6b21d814ced8c7daca32db9b5f3ef o_Data.mhk?cd
  94f573ba56bd11f6ea522ccd04bf44f81f0913fd o_data_french.mhk
  82d140db22f730258c454479379c07b2f4e50172 o_data_german.mhk
  cfb4c85771fea562717c3811e11b45b806a024e9 o_data_italian.mhk
  d2942808dd13654a337b7933ad3ce324f381a6c5 o_data_japanese.mhk
  a58498117e0f5ae376326320edd318e81732398f o_data_polish.mhk
  a0f77c46081bf9fa55ff8ce3e67a77458ce361fc o_data_russian.mhk
  23fa849fbcab312d8a549b0a6c589cce339a25ba o_data_spanish.mhk
  9ea6f7fb97a4d08269d376eefb6f6a8796004813 o_Sounds.mhk?cd
  a9777d5992e897038611468e0aef2a6ba58f2f4c p_Data.mhk?cd
  e20eb398beb9f7417d8202220bbfac7a45e520ac p_data_french.mhk
  a5561899a5d511d8e2db77e3770af4be50446848 p_data_german.mhk
  7eac9f59b458f7b1e6a80eac802f0a6567cc2589 p_data_italian.mhk
  1c21de6f1eacc55500068c1911710ea595c170c2 p_data_japanese.mhk
  b5c74b41216821a37a13cd6b7dcbdea8e8fa8e91 p_data_polish.mhk
  2e73ebca9c69aa5a2c8c78292698b75e6ba62ed6 p_data_russian.mhk
  e949f3073f0ee56ccdf9bb015f78ed35add9822a p_data_spanish.mhk
  4534e64423a2be856fd3ee656c03f1798a56f0bd p_Sounds.mhk?cd
  303dca424f497e0cf3ca70ffb128c1c276f6f33e r_Data.mhk?cd
  d2dc5dd7e854aaaeef0d7f9edcbd76f10bd8d0cf r_data_french.mhk
  d73b0d7b04f35117f357bac400cc65669c98fb4f r_data_german.mhk
  8003ca1b8ce69bbff4319da0a8109eaf3172d40e r_data_italian.mhk
  48b0e0d88fb02243385b63214791c127914e5c2c r_data_japanese.mhk
  78729d2e21297aaa7d9f3a5929906ee5b90d493c r_data_polish.mhk
  4819fc1b31f4e636440002f76487495f84292f49 r_data_russian.mhk
  9e79d8b0906c6a3ba566ea6ac375d0e9b566abf4 r_data_spanish.mhk
  0a1f0e33fed61ffd1802e46ce4c2dbef17a460b8 r_Sounds.mhk?cd
  1d1b88097976c2a21845bd2399c28d84f2e3e09a riven.exe
  e8d9a9a685b253d7ca4e2c2788df333ef20c0bef t_Data.mhk?cd
  127a3890fe5bcb9d8978ce15497650b8ca58b52f t_data_french.mhk
  93e34a781df8a28a3f9dcce3a837d1e9ef048ba9 t_data_german.mhk
  4e3db67311c4ca5c55af90f8e2f73f2353ed39a6 t_data_italian.mhk
  da08e09a384fb138f9bf525f2c676b3569e8cc91 t_data_japanese.mhk
  44b7073a42a451df8dad59ac16aeda4af80aa3b2 t_data_polish.mhk
  8044809eae238f4e17a4739d3d679e87f061b070 t_data_russian.mhk
  86ce449b2711b3b3e9f690b89e60f92e295988c0 t_data_spanish.mhk
  c2accd26714a76f641e0d99ef82b8e6e20fe9f80 t_Sounds.mhk?cd
  aae085ca50f6e555f4cefd745663faa53dbac1ed arcriven.z?cd
  b08b179a3853ef54906aaccd87ffebda4a3f4e87 b_Data1.mhk
  30d5f1d3dc345453cbb62e255b31ff82945c301d j_Data3.mhk
  0dd27cf1cdf9b30354fab15a321cc2237326a632 Manual.pdf?cd
  3394367206722d735bdb80572e217e6dce3034dc Extras.mhk
  cfe933cfd4e0f4e7f0c581f6faa1d4fc93736e92 a_Data.mhk?dvd
  33bce37d8c7973bde8aa7b3e2a30dc97ed7149d8 b2_data.mhk
  8c283a669755bd5ac216a47c4f8dd038efa4c714 b_Data.mhk?dvd
  31fba12fea441d319503e5d849b668bee93b281a b_Sounds.mhk?dvd
  b4b46cd612323b0ecb3de67ecbe1cc2febad1b4d g_Data.mhk?dvd
  2d7f6e9016b3ff563f2156ffb566f4672be986e4 g_Sounds.mhk?dvd
  79c9acd8327974cbbdf5c369e347d3890281b0b8 j_Data1.mhk?dvd
  14fc5f112a3cae288cd0c1d11412b9227d43c1a7 j_Data2.mhk?dvd
  38b4006378ef074c531af1ca8ea75d4bd45b9dac j_Sounds.mhk?dvd
  60496c425469b9d59c6154cab7594a78bd2cdf14 o_Data.mhk?dvd
  4b03f499e1f41bb251e5b8b396036204fcc6243b o_Sounds.mhk?dvd
  8fd57f3a8b5d62a1c5f10ca9c7c14ed391222bad p_Data.mhk?dvd
  e91e67a1e8926c8d19b18bed68d5b6b86d90e288 p_Sounds.mhk?dvd
  a7ede43c7be443ebfc7ed5fa334627166f82d74c r_Data.mhk?dvd
  48c750a85d4abcaa03b161fd17f68f67648593a5 r_Sounds.mhk?dvd
  f7d806375cbfd39c45a5324bdfef4ec72ae6735a t_Data1.mhk
  99990cb8a2da4ef0612c8384110c595c6dd23cc9 t_Data2.mhk
  ca53d58a956e7d63d98b42683a2a64a4d1405e75 t_Sounds.mhk?dvd
  7f60423b18f4b45955361c71144725b9c524dd85 Manual.pdf?dvd
  a2f0a13a4435c2548ebed01552a93491549df6d1 Readme.txt
  b93128ffed7d6394c0071161891ad4ed0cd97dee Read EULA First
  5e5e9c97cf1f52335893f7c8d731425ea915d1d0 arcriven.z?dvd
  77a84cafcb603c945dd882cf133caaea6997e8fe manual.pdf?gog
  b4518ba512a2c5ccda8233914cb66bf4373534ab readme.txt?gog
  d54bd03e2669deee4c396d99de95eba9b8218ba3 EULA_de-DE.txt?gog
  e39c93bf6297dfd9c5c2b758d7ed44f5192f7a39 EULA_en-US.txt?gog
  1debe3e56c44e8e2e5991d3c0123bd398ae5c4d5 EULA_fr-FR.txt?gog
  03dd481ba3aaea7b20980c928c93551e7e8e3687 EULA_pl-PL.txt?gog
  e415ee11ff34d0e39fb4904ac693de69c025a3b3 EULA_pt-BR.txt?gog
  0148f80fa404add34331cfc5412d5651dbd98a12 EULA_ru-RU.txt?gog
  74c59ce79c2523e177a5c7ab5dd272fc7a6ef07b EULA_zh-Hans.txt?gog


sha256sums: |
  9caa40397285fb2d15e3c3d3c166a8413bb1f10782e6a955abb6f42de2e6caa4 setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114).exe
  afa26b37a6dee63ea12de2ff28031bf9e7893b820937899c85ebdcb55ac4adb2 setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-1.bin
  5743b838349fb01f496282702d4dd31cf1044380df75c4f8e1e379191ad538a5 setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-2.bin
  70bf37038a48f18d21c161f14acd249467949f73bc85a25d3a6321fe6a461d65 FreeSans.ttf
  f13af2cc512cecbbad8cc7e6adbb1bd384695993f58725ff9344b9129051a140 LICENSE.mplus
  8ac1c23f6fe2c8ea4efd23cb1830f0dffd044288d9aecec717822d7fb639290f a_Data.mhk?cd
  a42767c3b272dd1f0c888fa050be25ed3397bb4659ec76b144b595557aecb3bc a_data_french.mhk
  1748b452a62992347b331826e94cca201ccc326602e8d3d26d6d67f75fd9ab13 a_data_german.mhk
  5f071288a29833f6fa793d098918c9168efff88e855a7e21f35e67c5b9babf87 a_data_italian.mhk
  f97adae15cb8677226ff30cad960cec2192c640ba1fc8b3a43aeac35b790612c a_data_japanese.mhk
  19535bc477f799c273b1c042551349789b904a2758d6b2d1c517d68cc0740d42 a_data_polish.mhk
  cab9230659ed6ee64a515039a9f2669289b22332456c833f90db868a7697b291 a_data_russian.mhk
  a96a558364b8a8ce30a371fd11cbe6946b25e2558ef31523d386754d10654334 a_data_spanish.mhk
  91e0b5f41a55da9a0d7307e682c60e5c5d028de61142708826bf8863e65b0932 a_Sounds.mhk
  8c2a88dac31a0220605d9427782da8d2dd2bc697cc4d91885cd17537650d4745 b_Data.mhk?cd
  e65df7c1f5ca6d8e2374e089e5f31cf7fae918acb7fb9081e7f5b2d98140f281 b_data_french.mhk
  23bb7b6a72d9922aaa9665c0c4b9a4c2a9f52615f8f4ef9856f432b21a13aa5d b_data_german.mhk
  4e4ae3fb3990cb36955d9ca40fb498f7e00ac261ca74584591000dc0b76cd02a b_data_italian.mhk
  1d37e1b41094f0b1f690e6dac8e4affb635eed7fbfaf68568c476d2405e4720b b_data_japanese.mhk
  aedd4051fff4fda2395c60ab17835169197e5a43b882115b02701d1dcf4ebe5d b_data_polish.mhk
  332dc4d660c7a28e8b50a128b009ef027a17c32f107fb71e216f9c37832611af b_data_russian.mhk
  277d2f9beaf837e06a5fdcde86669d0de43b9fdb0de10aee18a4e715a05d96b0 b_data_spanish.mhk
  caa6f9bae659265154e71ca0e0351cf687fdd2e0957e9191abb319262a7a5e93 b_Sounds.mhk?cd
  7c3a276c784bfaea5aa30188548b51bccce9ee3248a28fabb55a91562bff0537 g_Data.mhk?cd
  706309f56a61f6cf3fe687c3fb18d8c574444105d54ec417927f0a548d866378 g_data_polish.mhk
  4d66d7466cdd3d7ce0cacad751c7c751ac305d84c55b80339eca43b9cc4c0a11 g_Sounds.mhk?cd
  ec2b8cada0f560916cfba08145f08cb7eafa2985a9338c1915edb0dadc4eaa98 j_Data1.mhk?cd
  e69f8ee7d17c3bf6ee54183f5745a4b86401486854962d5d7e47672e8c85a644 j_Data2.mhk?cd
  ae3c5c6cc26df60f6c5c397831951bd60cfeb86849a0d7281e814f11fd7053aa j_data_french.mhk
  4eb57c343e4c8387cea08a13d526792da528047ec33395a027bf13d8fc9a19a5 j_data_german.mhk
  193b4fbfa4196d59fed14d584e2a8418d9c04ef3503911802f781abf1b910d3f j_data_italian.mhk
  bf1056deb8bc823f72393cb1618c59307846ed8da425faedb75ae09f1a67d80d j_data_japanese.mhk
  60d394e81cdfb105fd6dd6331ffca78d66ef8b88be925050d00e25692f5761c1 j_data_spanish.mhk
  f98f5d3fec6f6d1c8eaafceaee342684f4e15afbe7a19b244ce048eb9e9d0b5c j_Sounds.mhk?cd
  46a300441e46ac92011abd7bbd47802d12589c9805a57952fad9872dfb514feb mplus-2c-regular.ttf
  fbdeb926142896d44628b149a00c218ef8a7de3c1adeb31cca5def113129bbb5 o_Data.mhk?cd
  ca43ee97c1e78828b089c16b47e0e0ac10a95867ff4ed609d2f0f4fe8b61db23 o_data_french.mhk
  edea8b35ac37973ada458ccef34185a8457a0c922f600af81534f4d42cfe7f19 o_data_german.mhk
  e9a113e8f8bcb99442cf5eb654054cb5881dc97070f7fc699b0ff4756995dd95 o_data_italian.mhk
  49417f9304aa9e344911c219fa31ad129d5bac6b9e31e9ab81e80b9d3ba588d3 o_data_japanese.mhk
  5726ea9a04efacfec12919b2494f1c756eda49a86d9ba229225f9697e42a1024 o_data_polish.mhk
  0380bca1785452ca79d27a6ded9b90f05e18130aa192a935e0952d17704878d8 o_data_russian.mhk
  b1c4e7aeba1e8b0c7734d4a87921d3966179717bac5b8df5b0d840750f382141 o_data_spanish.mhk
  b2712b86bb284403a8b68f33d883339ce55ed6b100fe75bb1ed16d8da6996ae4 o_Sounds.mhk?cd
  b9b80bc8012c9c4248f6b3067d9cd9f40b37ea65eae0b3fe8e7d32888a043eaa p_Data.mhk?cd
  8e0e6858cfafd59b572461871e8758cf7cd9851d87bca5ff693ac1799043ad70 p_data_french.mhk
  5768925896930e63d1a3a74429e471183f7b5350a40e157e1f378d968eafe6cf p_data_german.mhk
  e15dbcd7badaa96889a0f293e7d9142159d6cb7189bb011fb2b37f9059495ab1 p_data_italian.mhk
  5a5e5a246b6ff5c7aaa52147d5048dda24dfdd0ebb55bb6c608aaee4f887489b p_data_japanese.mhk
  448e3a46f1f21f35e080ce45feffc1315d6e5b074f7441f41ac0f519a14b9f7f p_data_polish.mhk
  583f5cdb47040380994cc53150c66da2babaa7ed7023ec99c5222de33c8cfd14 p_data_russian.mhk
  b788fba25ef81a30b41ed1750c1b06bc2de4f5cf7c1c6badfc42619c1b5b6d29 p_data_spanish.mhk
  047e37af947a8be956c2486d97612ac5f2c48092d7ff24c9ebcc497e23ac67a8 p_Sounds.mhk?cd
  ab10561a7026eee98b9741de8237576a90c2243f8f8919ec175175f17774e803 r_Data.mhk?cd
  aae71d586df42f66a9c97ec2f26388834186aef5850b8fdfe3abb7be2a2ee26d r_data_french.mhk
  2e6b73ed23d095450b87a93ab13417920ad49a70f5691c7c6f3cfa6a4252c529 r_data_german.mhk
  2596e5e0aab881c624485b0a41f916587231795b724fde01d7664fa9c073ca50 r_data_italian.mhk
  6f002809e75f9783af7d322f604893006ecb9f44700d21b15139d200be604c63 r_data_japanese.mhk
  bec82eec99f2c652a40eb1e504d213d622665d7264c3e0926a8d7d68d2636e18 r_data_polish.mhk
  705ddf78da0bd65659d69331bf93c3959ed5b0fcc06bb0c070ef6f648d86f9c2 r_data_russian.mhk
  08f3bdc9bc1f2ebff4b5b932d6c87f854347c3f4c0f360bdb0f55df1de91209b r_data_spanish.mhk
  4f41f91049c657575fd028409c6bc42b11c4806507a85d2c967d4f405822ed3e r_Sounds.mhk?cd
  165d9f68305729db622bc1a0c9f7d0a77192df7420834c536e6c4a200c65d59a riven.exe
  725e0342efe159d296eb9cf690eb9dcbf3f30c7e1356df0201bccdd22cf22b05 t_Data.mhk?cd
  ee27ac5185c2335d4c7e4181884b9982f86de52acfee81826068345d08015bf7 t_data_french.mhk
  dc73850e1f79886c8537aea7ea130cc83e61de26da05f656c1b6a91c9e1e367d t_data_german.mhk
  bfce8da1d91a4a17221b39dbffcbcb831c1bb7eed6fc1e555eea4f8a7e85fbbc t_data_italian.mhk
  fff29f4f59ecaf062924432bd79efa2746048b5e231a87b439b93819b0dfd9b8 t_data_japanese.mhk
  e2edcb674b9899251e8a6468ff70d39a9716ad3de431904a95f5865a8f0822c9 t_data_polish.mhk
  ddc9eb6f46928523c36e2c93c971292f4c25a62ef04d43b04fd4f2ced442233c t_data_russian.mhk
  ab770ae803550d7b5caf6d8a7d50c37c568400f7b7afe57a7eb3581fd69697c1 t_data_spanish.mhk
  674ef1f0559645478fb2f15d334e61ddad3ce56741d9346de41f12ca316068a4 t_Sounds.mhk?cd
  114a233a170367959615c692d7a73f71739db7532cb70ef701b30b70bf1f38a1 arcriven.z?cd
  6f6c1c5539b5879e0e43dbc56c8c40010b9979e084ffefe2be1a1f691c0424f2 b_Data1.mhk
  ca0b4025a1df2f43f3dc7c31c56fc84d94d17cd7006ae3a433867080f2b59a5d j_Data3.mhk
  d0e0f3cfb3092ac626f1e1c1229da8e1e3569b0416c6f769a5aab53c74ae2541 Manual.pdf?cd
  f92209e543bb69c3b9d81d0eaec4b14ae469f42cbcf656eb42c995bb71f95e9e Extras.mhk
  6b94c82e617d54a02735ed2922f6374fdfe263a5a610e87421d1bb9dc75364e8 a_Data.mhk?dvd
  05445cb34e6b7adebd942ecf3fe38cd16125993b7e690ebd7d767d5a74e455f9 b2_data.mhk
  6f97b014583ed4c30b8afa048fbe7f30f701bb926e73dc21c55b15a29573eb1b b_Data.mhk?dvd
  6285674e763c8dce7a3d34e9b84357ee66fc2dcf46803429d59561cf094283b3 b_Sounds.mhk?dvd
  175630b996d8cb7494b1b4a0cd3549f378e318d65aca32902a92614a395fb443 g_Data.mhk?dvd
  7f1c0c0f04dd0030d1ca7845bf3b4cddc1f419073a4160ea43e1693787413fad g_Sounds.mhk?dvd
  d1f3e3a33705d7e92115ffda1dae707ab8096f104e0841991e3822c95c5ff96e j_Data1.mhk?dvd
  e75d17767875c28492547868eaa8f03cb291ade3ad11fc6eca4f07eede628112 j_Data2.mhk?dvd
  f203699d071c4a9426c8608198ee2fb055b99d01e31964387664f329baf3115a j_Sounds.mhk?dvd
  568ecf843d3c8df0375b209e24184e93f56c96848c6343fb84678d3fa967027a o_Data.mhk?dvd
  8e9b1618745af7e8f9d5ec70b71aaa256f972e7950ae967d50edf9f889b6e48d o_Sounds.mhk?dvd
  1a0f1e79c237da1dbd1cb236f157e2bab4bbc3b97ac1185c00723337a2bcca80 p_Data.mhk?dvd
  bb0dc82ab8e920606b0ad8526e78ed8f4773feda121c9eb4ae990c964ee95ff3 p_Sounds.mhk?dvd
  d3e5a249fcf657c88f760a66fa13adecf493a2be10150c3fc4c8e50047cb226a r_Data.mhk?dvd
  83e3c5c9e44b97e29b8a2002c6c648e9cd0eb70fd80be341134e846d8d7c46d5 r_Sounds.mhk?dvd
  44489e83eeb3f2b84acc160c3bcd1ae091bcf6d9021bad74ee4d97bb02f46ecb t_Data1.mhk
  be9fbb17977ada5381dd13f6fc690382bc97289813c02d2733087bb672512b68 t_Data2.mhk
  2588a0fdbdf7888aa79b03373657150fc67118931ce9529776ca224f92aad2f0 t_Sounds.mhk?dvd
  359c7a2cfc24d2032dd2823944603b6dabd24343169eb8a473d2b753c89a3f8b Manual.pdf?dvd
  fc7391191311540d875afe68c38773c98cf646764e153fe370c5f1dfccd1d3b2 Readme.txt
  4b4dbfb6a8414b7a296cafda446612377f6cd4a98103e89cf3dd12af2f9d6405 Read EULA First
  c7f30e31afb61887a437eab49b17ad8156c57d41f7a183636b32d97217cddabe arcriven.z?dvd
  893681887835b14f78d1a52b691e30ec5e29634ef837ec549b0678b77676a85d manual.pdf?gog
  c4922c43d2fee240fd737a47f1a1696fa3035107b560f1c688bd79cdc2edb3a3 readme.txt?gog
  d1cfe320cb6081bdace9398d7bfb7878ef84a81bd1dc24648133c97171e0be68 EULA_de-DE.txt?gog
  92f8b49d186c49c154bee5eae025934a896b26a0836ae3147e0488c2a1eb3a2f EULA_en-US.txt?gog
  3efaccd7a6adec010c92646d3d140c54fafb05318a1e923278086c04c484b5f1 EULA_fr-FR.txt?gog
  4a076da0a7a713665ed42fe0a8be6bab13083c43ffff121b8b829009a9c86f74 EULA_pl-PL.txt?gog
  8088039c555792603480bcc09546a1570053bd7ead5263931deeb9eca9896421 EULA_pt-BR.txt?gog
  20756e9f49e29f3300925c509b4b05b9482a42e3738297b98007a6eae7a12a40 EULA_ru-RU.txt?gog
  5c7c671b4c43beaae272bd73b5fd1036137f5215bd8b9352409adc997f92653d EULA_zh-Hans.txt?gog

...
